rem Builds the glpkjni.dll on a windows machine
rem ===========================================
rem Author: Michael Schober Date: 07/30/2010
rem We need the correct paths! Please check the
rem following paths, before running the script.

rem Adding MinGW to the path variable ...

rem IMPORTANT for Building GLPK with MINGW64
rem sh configure --disable-reentrant; make

set PATH=C:\MINGW\mingw64-x64;%PATH%


rem Setting path to Java SDK
set JAVASDK="C:\MINGW\jdk1.8.0_161"

rem Compiling ...
gcc -fPIC -c -I%JAVASDK%\include -I%JAVASDK%\include\win32 -Ilib\win_64 lib\win_64\lpx.c 
gcc -fPIC -c -I%JAVASDK%\include -I%JAVASDK%\include\win32 -Ilib\win_64 lib\win_64\glpk_jni.c	
gcc -fPIC --shared -Wl,--kill-at -I%JAVASDK%\include\ -I%JAVASDK%\include\win32\ glpk_jni.o lpx.o lib\win_64\libglpk.a lib\win_64\libltdl.dll.a -lm lib\win_64\libz.a C:\WINDOWS\system32\kernel32.dll -o glpkjni_x64.dll

rem Cleaning up ...
del glpk_jni.o
move glpkjni_x64.dll build