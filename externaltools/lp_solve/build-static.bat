@echo off

REM ---------------------------------------------------------------
REM This is a build file for the lp_solve Java wrapper stub library
REM on Windows platforms.
REM
REM REquirements:
REM
REM - GNU C++ Compiler (g++)
REM - Java Development Kit > 1.4.x installed
REM - JAVA_HOME environment variable set
REM - lp_solve libraries built
REM
REM Change the paths below this line and you should be ready to go!
REM ---------------------------------------------------------------

rem -- Set the path to the lp_solve directories here!
set LPSOLVE_DIR=src\lp_solve_5.5
set JAVA_HOME=C:\Programme\Java\jdk1.6.0_19

set c=gcc
set cpp=g++

REM determine platform (win32/win64)
echo main(){printf("SET PLATFORM=win%%d\n", (int) (sizeof(void *)*8));}>platform.c
%c% platform.c -o platform.exe
del platform.c
platform.exe > platform.bat
del platform.exe
call platform.bat
del platform.bat

REM -- Here we go !
REM --
set SRC_DIR=src\lp_solve_5.5_java\src\c
REM The 64 bit java sdk also uses win32 as folder
SET INCLUDES=-I%JAVA_HOME%\include -I%JAVA_HOME%\include\win32 -I%LPSOLVE_DIR% -I%SRC_DIR%
SET LIBS=%LPSOLVE_DIR%\lpsolve55\bin\%PLATFORM%\liblpsolve55.a

REM -- The '-mrtd' switch adds __stdcall to every method
%cpp% %INCLUDES% -mrtd -c %SRC_DIR%\lpsolve5j.cpp
%cpp% -fPIC --shared -W1 -Wl,--kill-at %INCLUDES% lpsolve5j.o %LIBS% -o build\lpsolve55j.dll
del lpsolve5j.o

SET PLATFORM=

